# Ubuntu Touch Tweak Tool

## Building

<a href="https://clickable-ut.dev/en/latest/install.html"><img align="right" src="https://clickable-ut.dev/en/latest/_static/logo.png" alt="Clickable" style="float:right;margin:50px" /></a>

Install [Clickable](https://clickable-ut.dev/en/latest/install.html).

Build the app:

```bash
$ clickable
```

## Download

<a href="https://open-store.io/app/ut-tweak-tool.sverzegnassi"><img align="right" src="https://open-store.io/badges/en_US.png" alt="OpenStore" /></a>

Downloads of the current (and previous) versions are available from the [Releases](https://gitlab.com/myii/ut-tweak-tool/-/releases) page.

The latest version is available on the [OpenStore](https://open-store.io/app/ut-tweak-tool.sverzegnassi).

## Translation

Translations are very welcome; please submit these using [Weblate](https://hosted.weblate.org/engage/ut-tweak-tool/).

<a href="https://hosted.weblate.org/engage/ut-tweak-tool/"><img src="https://hosted.weblate.org/widgets/ut-tweak-tool/-/ut-tweak-tool/multi-auto.svg" alt="Translation status" /></a>
